package inventaire.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class InventaireException extends Exception {

    /**
     * Constructeur de la classe InventaireException
     *
     * @param message Message d'exception
     */
    public InventaireException(String message){ super("\nInventaireException: " + message); }
}
