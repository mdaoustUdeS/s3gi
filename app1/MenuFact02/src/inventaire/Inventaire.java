package inventaire;

import ingredients.Ingredient;
import java.util.ArrayList;

import ingredients.IngredientInventaire;
import inventaire.exceptions.InventaireException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 *
 * L'inventaire est un Design Pattern Singleton
 */
public class Inventaire {
    private ArrayList<IngredientInventaire> lesIngredients = new ArrayList<>();
    private volatile static Inventaire instanceInventaire;

    /**
     * @return l'instance d'inventaire
     * @throws InventaireException Exception lorsqu'une autre instance d'inventaire est appelée
     */
    public static Inventaire getInstance() throws InventaireException {
        if (instanceInventaire == null) {
            synchronized (Inventaire.class) {
                if (instanceInventaire == null)
                    instanceInventaire = new Inventaire();
            }
        }
        else {
            throw new InventaireException("L'inventaire existe déjà.");
        }

        return instanceInventaire;
    }

    /**
     * @param ingredient Ingrédient pour plat au menu
     */
    public void ajouter (IngredientInventaire ingredient)
    {
        lesIngredients.add(ingredient);
    }
}
