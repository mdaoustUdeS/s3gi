package menufact.plats;

import menufact.plats.PlatAuMenu;
import ingredients.IngredientInventaire;
import org.junit.Test;
import static org.junit.Assert.*;


public class PlatAuMenuTest {

    @Test
    public void testPrintPlatAuMenu() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testPrintPlatSanté() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testPrintPlatEnfant() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }
}
