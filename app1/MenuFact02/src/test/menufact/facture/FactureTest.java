package menufact.facture;

import org.junit.Test;
import static org.junit.Assert.*;


public class FactureTest {

    @Test
    public void testAjoutePlatOuverte() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testAjoutePlatFermee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testAjoutePlatPayee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testSousTotalZeroPlats() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testSousTotalUnPlats() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testSousTotalDeuxPlats() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testTps() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testTvq() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testTotal() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testAfficherFactureOuverte() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testAfficherFactureFermée() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testAfficherFacturePayee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testModificationContenuFacture() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatOuverteOuverte() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatOuverteFermee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatOuvertePayee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatFermeeOuverte() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatFermeeFermee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatFermeePayee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatPayeeOuverte() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatPayeeFermee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testEtatPayeePayee() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }
}
