package ingredients;

import org.junit.Test;
import static org.junit.Assert.*;

public class IngredientInventaireTest {

    @Test
    public void testQuantitePositive() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testQuantiteZero() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testQuantiteNegative() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }
}
