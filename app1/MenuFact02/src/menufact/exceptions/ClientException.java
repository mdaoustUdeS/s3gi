package menufact.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class ClientException extends Exception {

    /**
     * Constructeur de la classe ClientException
     *
     * @param message Message d'exception
     */
    public ClientException(String message){
        super("\nClientException: " + message);
    }
}
