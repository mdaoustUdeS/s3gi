package menufact.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class MenuException extends Exception {

    /**
     * Constructeur de la classe MenuException
     *
     * @param message Message d'exception
     */
    public MenuException(String message){
        super("\nMenuException: " + message);
    }
}

