package menufact;

import menufact.exceptions.ClientException;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClientTest {

    @Test
    public void testToString() throws ClientException {
        Client client = new Client(1,"Mr Client","1234567890");
        String result = client.toString();
        assertEquals("menufact.Client{idClient=1, nom='Mr Client', numeroCarteCredit='1234567890'}", result);
    }
}
