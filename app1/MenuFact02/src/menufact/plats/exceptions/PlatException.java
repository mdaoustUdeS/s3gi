package menufact.plats.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class PlatException extends Exception {

    /**
     * Constructeur de la classe PlatException
     *
     * @param message Message d'exception
     */
    public PlatException(String message){
        super("\nPlatException: " + message);
    }
}
