package menufact.plats;

import menufact.plats.exceptions.PlatException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 *
 * Interface pour le Design Pattern State du plat choisi
 */
public interface PlatChoisiEtat {

    /**
     * @param   platChoisi      Plat choisi au menu
     * @throws  PlatException   Exception lorsque le plat ne peut plus changer d'état
     */
    void prochainEtat(PlatChoisi platChoisi) throws PlatException;
}
