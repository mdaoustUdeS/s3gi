package menufact.plats;

import ingredients.IngredientInventaire;
import menufact.plats.exceptions.PlatException;

public class PlatAuMenu {
    private int code;
    private String description;
    private double prix;
    private IngredientInventaire[] ingredientInventaire;

    public PlatAuMenu(int code, String description, double prix) {
        this.code = code;
        this.description = description;
        this.prix = prix;
    }

    public PlatAuMenu(int code, String description, double prix, IngredientInventaire[] ingredientInventaire) {
        this.code = code;
        this.description = description;
        this.prix = prix;
        this.ingredientInventaire = ingredientInventaire;
    }

    public PlatAuMenu() {
    }

    @Override
    public String toString() {
        return "menufact.plats.PlatAuMenu{" +
                "code=" + code +
                ", description='" + description + '\'' +
                ", prix=" + prix +
                "}\n";
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) throws PlatException {
        if(prix < 0) {
            throw new PlatException("Le prix ne peut pas être négatif");
        }
        else
            this.prix = prix;
    }
}
