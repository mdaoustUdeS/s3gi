package menufact.plats;

import menufact.plats.exceptions.PlatException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Impossible implements PlatChoisiEtat {

    /**
     * @param platChoisi Plat choisi
     * @throws PlatException Exception lorsque le plat ne peut plus changer d'état
     */
    @Override
    public void prochainEtat(PlatChoisi platChoisi) throws PlatException {
        throw new PlatException("Le plat est impossible à servir, il ne peut plus changer d'état.");
    }

    /**
     * @return l'état du plat choisi
     */
    @Override
    public String toString() {
        return "IMPOSSIBLE À SERVIR";
    }
}
