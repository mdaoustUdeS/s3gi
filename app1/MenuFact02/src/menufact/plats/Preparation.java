package menufact.plats;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Preparation implements PlatChoisiEtat {

    /**
     * @param platChoisi Plat choisi
     */
    @Override
    public void prochainEtat(PlatChoisi platChoisi) {
        platChoisi.setEtat(new Termine());
        System.out.println("\nLe plat choisi est terminé!");
    }

    /**
     * @return l'état du plat choisi
     */
    @Override
    public String toString() {
        return "EN PRÉPARATION";
    }
}
