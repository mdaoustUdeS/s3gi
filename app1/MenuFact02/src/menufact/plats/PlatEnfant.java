package menufact.plats;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class PlatEnfant extends PlatAuMenu {
    private double proportion;

    public PlatEnfant() {}

    public PlatEnfant(int code, String description, double prix, double proportion) {
        super(code, description, prix);
        this.proportion = proportion;
    }

    public double getProportion() {
        return proportion;
    }

    @Override
    public String toString() {
        return "PlatEnfant{" +
                "proportion=" + proportion +
                "} " + super.toString();
    }
}
