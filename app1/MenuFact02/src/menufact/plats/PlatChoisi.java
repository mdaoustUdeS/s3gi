package menufact.plats;

import menufact.plats.exceptions.PlatException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class PlatChoisi {
    private PlatAuMenu plat;
    private int quantite;
    private PlatChoisiEtat etat;

    /**
     * Constructeur de la classe PlatChoisi
     *
     * @param   plat            Plat choisi au menu
     * @param   quantite        Quantité du plat choisi au menu
     * @throws  PlatException   Exception lorsque la création du plat choisi ne fonctionne pas
     */
    public PlatChoisi(PlatAuMenu plat, int quantite) throws PlatException {
        if(plat != null && quantite > 0) {
            this.plat = plat;
            this.quantite = quantite;
            this.etat = new Commande();
            System.out.println("\nLe plat choisi est commandé!");
        }
        else {
            throw new PlatException("Il est impossible de créer le plat choisi avec les paramètres indiqués");
        }
    }

    /**
     * @return le plat choisi
     */
    public PlatAuMenu getPlat() {
        return plat;
    }

    /**
     * @return la quantité du plat choisi
     */
    public int getQuantite() {
        return quantite;
    }

    /**
     * @param quantite Quantité du plat choisi
     */
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /**
     * @return l'état du plat choisi
     */
    public PlatChoisiEtat getEtat()
    {
        return etat;
    }

    /**
     * @param etat État du plat choisi
     */
    public void setEtat(PlatChoisiEtat etat){
        this.etat = etat;
    }

    /**
     * @throws PlatException Exception lorsque le plat ne peut plus changer d'état
     */
    public void prochainEtat() throws PlatException {
        etat.prochainEtat(this);
    }

    /**
     * @return les informations du client
     */
    @Override
    public String toString() {
        return "menufact.plats.PlatChoisi{" +
                "quantite=" + quantite +
                ", etat=" + etat +
                ", plat=" + plat +
                '}';
    }
}
