package menufact.chef;

import menufact.plats.PlatChoisi;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 *
 * Interface pour le Design Pattern Observer de la facture
 */
public interface Observateur {

   /**
    * @param platChoisi Plat choisi au menu
    */
   void envoyerMessage(PlatChoisi platChoisi);
}
