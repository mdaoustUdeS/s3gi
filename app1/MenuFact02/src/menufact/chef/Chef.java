package menufact.chef;

import menufact.chef.exceptions.ChefException;
import menufact.plats.PlatChoisi;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Chef implements Observateur {
    private String nom;

    /**
     * Constructeur de la classe Chef
     *
     * @param   nom             Nom du chef
     * @throws  ChefException   Exception lorsque la création du chef ne fonctionne pas
     */
    public Chef(String nom) throws ChefException {
        if(!nom.equals("")) {
            this.nom = nom;
        }
        else {
            throw new ChefException("Le nom du chef est manquant");
        }
    }

    /**
     * @param platChoisi Plat choisi au menu
     */
    @Override
    public void envoyerMessage(PlatChoisi platChoisi) {
            System.out.println("\nLe chef a été notifier de préparer le plat " + platChoisi);
    }
}
