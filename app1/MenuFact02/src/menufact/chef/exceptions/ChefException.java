package menufact.chef.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class ChefException extends Exception {

    /**
     * Constructeur de la classe ChefException
     *
     * @param message Message d'exception
     */
    public ChefException(String message){ super("\nChefException: " + message); }
}
