package menufact;

import menufact.exceptions.MenuException;
import menufact.plats.PlatAuMenu;
import java.util.ArrayList;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Menu {
    private String description;
    private int courant;
    private ArrayList<PlatAuMenu> plat = new ArrayList<>();

    /**
     * Constructeur de la classe Menu
     *
     * @param   description     Description du menu
     * @throws  MenuException   Exception lorsque la création du menu ne fonctionne pas
     */
    public Menu(String description) throws MenuException {
        if(!description.equals("")) {
            this.description = description;
        }
        else {
            throw new MenuException("La description du menu est manquante");
        }
    }

    /**
     * @param p Plat au menu
     */
    void ajoute(PlatAuMenu p)
    {
        plat.add(p);
    }

    /**
     * @param i Plat courant
     */
    public void position(int i)
    {
        courant = i;
    }

    /**
     * @return le plat courant
     */
    public PlatAuMenu platCourant()
    {
        return plat.get(courant);
    }

    /**
     * @throws MenuException Exception lorsque le nombre maximal de plats est dépassé
     */
    public void positionSuivante() throws MenuException
    {
        if (courant+1 >= plat.size())
            throw new MenuException("On dépasse le nombre maximal de plats");
        else
            courant++;
    }

    /**
     * @throws MenuException Exception lorsque le nombre minimal de plats est dépassé
     */
    public void positionPrecedente() throws MenuException
    {
        if (courant-1 < 0)
            throw new MenuException("On dépasse le nombre minimal de plats");
        else
            courant--;
    }

    /**
     * @return les informations du menu
     */
    @Override
    public String toString() {
        return "menufact.Menu{" +
                "description='" + description + '\'' +
                ", courant=" + courant +
                ", plat=" + "\n" + plat +
                '}';
    }
}
