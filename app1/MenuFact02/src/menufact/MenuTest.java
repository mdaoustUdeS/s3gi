package menufact;

import menufact.exceptions.MenuException;
import menufact.plats.PlatAuMenu;
import org.junit.Test;
import static org.junit.Assert.*;

public class MenuTest {

    @Test
    public void testAjoute() throws MenuException {
        Menu menu = new Menu("menufact.Menu");
        PlatAuMenu p1 = new PlatAuMenu(0,"PlatAuMenu0",10);
        menu.ajoute(p1);
        String result = menu.toString();
        assertEquals("menufact.Menu{description='menufact.Menu', courant=0, plat=\n" +
                "[menufact.plats.PlatAuMenu{code=0, description='PlatAuMenu0', prix=10.0}\n" +
                "]}", result);
    }

    @Test
    public void testPrintMenuComplet() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testPrintMenuSelectionCode() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testGetInformationsPlat() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }

    @Test
    public void testChangeMenu() {
        String result = "Bad Output";
        assertEquals("Good Output", result);
    }
}
