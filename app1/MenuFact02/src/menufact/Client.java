package menufact;

import menufact.exceptions.ClientException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Client {
    private int idClient;
    private String nom;
    private String numeroCarteCredit;

    /**
     * Constructeur de la classe Client
     *
     * @param   idClient            Identifiant unique du client
     * @param   nom                 Nom du client
     * @param   numeroCarteCredit   Numéro de carte de crédit du client
     * @throws  ClientException     Exception lorsque la création du client ne fonctionne pas
     */
    public Client(int idClient, String nom, String numeroCarteCredit) throws ClientException {
        if(idClient > 0 && !nom.equals("") && !numeroCarteCredit.equals("")) {
            this.idClient = idClient;
            this.nom = nom;
            this.numeroCarteCredit = numeroCarteCredit;
        }
        else {
            throw new ClientException("Il est impossible de créer le client avec les paramètres indiqués");
        }
    }

    /**
     * @return l'identifiant unique du client
     */
    public int getIdClient() {
        return idClient;
    }

    /**
     * @param idClient Identifiant unique du client
     */
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    /**
     * @return le nom du client
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom Nom du client
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return le numéro de carte de crédit du client
     */
    public String getNumeroCarteCredit() {
        return numeroCarteCredit;
    }

    /**
     * @param numeroCarteCredit Numéro de carte de crédit du client
     */
    public void setNumeroCarteCredit(String numeroCarteCredit) {
        this.numeroCarteCredit = numeroCarteCredit;
    }

    /**
     * @return les informations du client
     */
    @Override
    public String toString() {
        return "menufact.Client{" +
                "idClient=" + idClient +
                ", nom='" + nom + '\'' +
                ", numeroCarteCredit='" + numeroCarteCredit + '\'' +
                '}';
    }
}