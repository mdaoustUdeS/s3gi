package menufact;

import inventaire.Inventaire;
import inventaire.exceptions.InventaireException;
import menufact.chef.exceptions.ChefException;
import menufact.exceptions.ClientException;
import menufact.exceptions.MenuException;
import menufact.facture.Facture;
import menufact.facture.exceptions.FactureException;
import menufact.plats.PlatAuMenu;
import menufact.plats.PlatChoisi;
import menufact.plats.PlatEnfant;
import menufact.plats.PlatSante;
import menufact.plats.exceptions.PlatException;
import menufact.chef.Chef;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class TestMenuFact02Custom {

    public static void main(String[] args) throws ClientException, MenuException, ChefException {

        boolean trace = true;
        TestMenuFact02Custom test = new TestMenuFact02Custom();



        /* CRÉATION DES PLATS */
        PlatAuMenu p1 = new PlatAuMenu(0,"PlatAuMenu0",10);
        PlatAuMenu p2 = new PlatAuMenu(1,"PlatAuMenu1",20);

        PlatSante ps1 = new PlatSante(10,"PlatSante0",10,11,11,11);
        PlatSante ps2 = new PlatSante(11,"PlatSante1",20,11,11,11);

        PlatEnfant pe1 = new PlatEnfant(10,"PlatSante0",10,2);
        PlatEnfant pe2 = new PlatEnfant(11,"PlatSante1",20,4);



        /* CRÉATION DU MENU */
        Menu m1 = new Menu("Menu 1");

        try {
            Menu m2 = new Menu("");
        } catch (MenuException e) {
            System.out.println(e.getMessage());
        }



        /* CRÉATION DE LA FACTURE */
        Facture f1 = new Facture("Facture 1");



        /* CRÉATION DU CHEF */
        Chef chef1 = new Chef("Ricardo");
        f1.ajouterObservateur(chef1);// Ajout du chef dans les observateurs du Message Publisher

        try {
            Chef chef2 = new Chef("");
        } catch (ChefException e) {
            System.out.println(e.getMessage());
        }



        /* CRÉATION DU CLIENT */
        Client client1 = new Client(1,"Mr Client","1234567890");

        try {
            Client client2 = new Client(1,"","");
        } catch (ClientException e) {
            System.out.println(e.getMessage());
        }



        /* GET INVENTAIRE */
        try {
            Inventaire inventaire1 = Inventaire.getInstance();
            Inventaire inventaire2 = Inventaire.getInstance();
        } catch (InventaireException e) {
            System.out.println(e.getMessage());
        }



        /* AJOUT PLATS MENU */
        test.test1_AfficherPlatsAuMenu(trace, p1,p2);
        test.test2_AfficherPlatsSante(trace, ps1,ps2);
        test.test3_AfficherPlatsEnfant(trace, pe1,pe2);
        test4_AjoutMenu(trace, m1);
        test.test5_AjoutMenuPlatsAuMenu(trace, m1, p1, p2, ps1, ps2, pe1, pe2);



        /* AJOUT CLIENT FACTURE */
        test.test7_AjouterClientFacture(f1, client1);



        /* AJOUT PLAT FACTURE */
        try {
            test.test6_CreerFacture(f1, m1);
        } catch (PlatException e) {
            System.out.println(e.getMessage());
        }



        /* PAYER FACTURE */
        test.test8_PayerFacture(f1);



        /* RÉOUVRIR FACTURE */
        try {
            f1.ouvrir();
        } catch (FactureException e) {
            System.out.println(e.getMessage());
        }
    }

    private void test1_AfficherPlatsAuMenu(boolean trace, PlatAuMenu p1, PlatAuMenu p2)
    {
        System.out.println("=== TEST 1 - Afficher plats AU MENU");
        if(trace)
        {
            System.out.println(p1);
            System.out.println(p2);
        }
    }

    private void test2_AfficherPlatsSante(boolean trace, PlatSante ps1, PlatSante ps2)
    {
        System.out.println("=== TEST 2 - Afficher plats SANTÉ");

        if(trace)
        {
            System.out.println(ps1);
            System.out.println(ps2);
        }
    }

    private void test3_AfficherPlatsEnfant(boolean trace, PlatEnfant pe1, PlatEnfant pe2)
    {
        System.out.println("=== TEST 3 - Afficher plats ENFANT");

        if(trace)
        {
            System.out.println(pe1);
            System.out.println(pe2);
        }
    }

    private static void test4_AjoutMenu(boolean trace, Menu m1)
    {
        System.out.println("=== TEST 4 - Ajout MENU");

        if(trace) {
            System.out.println(m1);
        }
    }

    private void test5_AjoutMenuPlatsAuMenu(boolean trace, Menu m1,
                                        PlatAuMenu p1, PlatAuMenu p2,
                                        PlatSante ps1, PlatSante ps2,
                                        PlatEnfant pe1, PlatEnfant pe2)
    {
        System.out.println("=== TEST 5 - Ajouter PLATS AU MENU");
        System.out.println("=== Ajout de plats au menu 1");
        m1.ajoute(p1);
        m1.ajoute(p2);
        m1.ajoute(ps1);
        m1.ajoute(ps2);
        m1.ajoute(pe1);
        m1.ajoute(pe2);

        if(trace) {
            System.out.println(m1);
        }
    }

    private void test6_CreerFacture(Facture f1, Menu m1) throws PlatException {
        System.out.println("=== TEST 6 - Créer FACTURE");

        PlatChoisi p1 = new PlatChoisi(m1.platCourant(),5);

        try {
            PlatChoisi p2 = new PlatChoisi(m1.platCourant(),0);
        }
        catch (PlatException e) {
            System.out.println(e.getMessage());
        }

        try {
            f1.ajoutePlat(p1);
        }
        catch (FactureException e) {
            System.out.println(e.getMessage());
        }

        // ************* CHANGER ÉTAT PLAT **************
        try {
            System.out.println(p1);
            p1.prochainEtat();
            System.out.println(p1);
            p1.prochainEtat();
            System.out.println(p1);
            p1.prochainEtat();
            System.out.println(p1);
            p1.prochainEtat();
            System.out.println(p1);
            p1.prochainEtat();
            System.out.println(p1);
        } catch (PlatException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(f1);
    }

    private void test7_AjouterClientFacture(Facture f1,Client c1) {
        System.out.println("=== TEST 7 - Ajouter CLIENT à la facture");
        f1.associerClient(c1);
        System.out.println(f1);
    }

    private void test8_PayerFacture(Facture f1) {
        System.out.println("=== TEST 8 - PAYER Facture");

        System.out.println("AVANT Payer");
        System.out.println(f1);
        f1.payer();
        System.out.println("APRÈS Payer");
        System.out.println(f1);
    }
}
