package menufact.facture;

import menufact.facture.exceptions.FactureException;
import menufact.plats.exceptions.PlatException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 *
 * Interface pour le Design Pattern State de la facture
 */
public interface FactureEtat {

    /**
     * @param   facture             Facture
     * @throws  FactureException    Exception lorsque la facture est PAYÉE
     */
    void setOuverte(Facture facture) throws FactureException;

    /**
     * @param   facture             Facture
     * @throws  FactureException    Exception lorsque la facture est PAYÉE
     */
    void setFermee(Facture facture) throws FactureException;

    /**
     * @param facture Facture
     */
    void setPayee(Facture facture);
}
