package menufact.facture;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Fermee implements FactureEtat {

    /**
     * @param facture Facture
     */
    @Override
    public void setOuverte(Facture facture) {
        System.out.println("\nLa facture est ouverte.");
        facture.setEtat(new Ouverte());
    }

    /**
     * @param facture Facture
     */
    @Override
    public void setFermee(Facture facture) {
        System.out.println("\nLa facture est fermée.");
        facture.setEtat(this);
    }

    /**
     * @param facture Facture
     */
    @Override
    public void setPayee(Facture facture) {
        System.out.println("\nLa facture est payée.");
        facture.setEtat(new Payee());
    }

    /**
     * @return l'état de la facture
     */
    public String toString(){
        return "FERMÉE";
    }
}
