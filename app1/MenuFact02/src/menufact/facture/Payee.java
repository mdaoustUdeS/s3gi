package menufact.facture;

import menufact.facture.exceptions.FactureException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Payee implements FactureEtat {

    /**
     * @param   facture             Facture
     * @throws  FactureException    Exception lorsque la facture est PAYÉE
     */
    @Override
    public void setOuverte(Facture facture) throws FactureException {
        throw new FactureException("La facture ne peut pas être réouverte.");
    }

    /**
     * @param   facture             Facture
     * @throws  FactureException    Exception lorsque la facture est PAYÉE
     */
    @Override
    public void setFermee(Facture facture) throws FactureException {
        throw new FactureException("La facture ne peut pas être refermée.");
    }

    /**
     * @param facture Facture
     */
    @Override
    public void setPayee(Facture facture) {
        System.out.println("\nLa facture est payée.");
        facture.setEtat(this);
    }

    /**
     * @return l'état de la facture
     */
    public String toString(){
        return "PAYÉE";
    }
}
