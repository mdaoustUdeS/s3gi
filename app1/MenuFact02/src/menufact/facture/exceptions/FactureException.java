package menufact.facture.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class FactureException extends Exception{

    /**
     * Constructeur de la classe FactureException
     *
     * @param message Message d'exception
     */
    public FactureException(String message){
        super("\nFactureException: " + message);
   }
}
