package menufact.facture;

import menufact.Client;
import menufact.chef.Observateur;
import menufact.chef.exceptions.ChefException;
import menufact.facture.exceptions.FactureException;
import menufact.plats.PlatChoisi;

import java.util.ArrayList;
import java.util.Date;

/**
 * Une facture du systeme Menufact
 * @author Domingo Palao Munoz
 * @version 1.0
 *
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Facture {
    private Date date;
    private String description;
    private FactureEtat etat;
    private ArrayList<PlatChoisi> platchoisi = new ArrayList<>();
    private int courant;
    private Client client;

    /** Liste des observateurs de la facture pour le Design Pattern Observer */
    private ArrayList<Observateur> observateurs = new ArrayList<>();

    /************ Constantes ************/
    private final double TPS = 0.05;
    private final double TVQ = 0.095;

    /**
     * Constructeur de la classe Facture
     *
     * @param description Description de la Facture
     */
    public Facture(String description) {
        date = new Date();
        etat = new Ouverte();
        courant = -1;
        this.description = description;
    }

    /**
     * @param client Client de la facture
     */
    public void associerClient (Client client)
    {
        this.client = client;
    }

    /**
     * Calcul du sous total de la facture
     * @return le sous total
     */
    public double sousTotal()
    {
        double soustotal=0;
         for (PlatChoisi p : platchoisi)
             soustotal += p.getQuantite() * p.getPlat().getPrix();
        return soustotal;
    }

    /**
     * @return le total de la facture
     */
    public double total(){
        return sousTotal()+tps()+tvq();
    }

    /**
     * @return la valeur de la TPS
     */
    private double tps(){
        return TPS*sousTotal();
    }

    /**
     * @return la valeur de la TVQ
     */
    private  double tvq(){
        return TVQ*(TPS+1)*sousTotal();
    }

    /**
     * Permet de changer l'état de la facture à OUVERTE
     * @throws FactureException Exception lorsque la facture est PAYÉE
     */
    public void ouvrir() throws FactureException
    {
        etat.setOuverte(this);
    }

    /**
     * Permet de changer l'état de la facture à FERMÉE
     * @throws FactureException Exception lorsque la facture est PAYÉE
     */
    public void fermer() throws FactureException
    {
        etat.setFermee(this);
    }

    /**
     * Permet de changer l'état de la facture à PAYÉE
     */
    public void payer()
    {
        etat.setPayee(this);
    }

    /**
     * @return l'état de la facture
     */
    public FactureEtat getEtat()
    {
        return etat;
    }

    /**
     * Méthode pour le Design Pattern State permettant de fixer un état à la facture
     * @param etat État de la facture
     */
    public void setEtat(FactureEtat etat){
        this.etat = etat;
    }

    /**
     * @param p Plat choisi au menu
     * @throws FactureException Exception lorsque la facture n'est pas OUVERTE
     */
    public void ajoutePlat(PlatChoisi p) throws FactureException {
        if (etat.toString().equals("OUVERTE")) {
            platchoisi.add(p);
            notifierObservateurs(p);
        } else
            throw new FactureException("On peut ajouter un plat seulement sur une facture OUVERTE.");
    }

    //*** Il faudra implémenter ici une méthode public void verifierIngredients()

    /**
     * @param observateur Observateur (Chef) de la Facture
     */
    public void ajouterObservateur(Observateur observateur) {
        observateurs.add(observateur);
    }

    /**
     * @param observateur Observateur (Chef) de la Facture
     */
    public void retirerObservateur(Observateur observateur) {
        observateurs.remove(observateur);
    }

    /**
     * @param p Plat choisi au menu
     */
    public void notifierObservateurs(PlatChoisi p) {
        for(Observateur observateur: observateurs) {
            observateur.envoyerMessage(p);
        }
    }

    /**
     * @return le contenu de la facture en chaîne de caractères
     */
    @Override
    public String toString() {
        return "menufact.facture.Facture{" +
                "date=" + date +
                ", description='" + description + '\'' +
                ", etat=" + etat +
                ", platchoisi=" + platchoisi +
                ", courant=" + courant +
                ", client=" + client +
                ", TPS=" + TPS +
                ", TVQ=" + TVQ +
                '}';
    }

    /**
     * @return une chaîne de caractères avec la facture à imprimer
     */
    public String genererFacture()
    {
        String lesPlats = new String();
        String factureGenere = new String();

        int i =1;


        factureGenere =   "Facture generee.\n" +
                          "Date:" + date + "\n" +
                          "Description: " + description + "\n" +
                          "Client:" + client.getNom() + "\n" +
                          "Les plats commandes:" + "\n" + lesPlats;

        factureGenere += "Seq   Plat         Prix   Quantite\n";
        for (PlatChoisi plat : platchoisi)
        {
            factureGenere +=  i + "     " + plat.getPlat().getDescription() +  "  " + plat.getPlat().getPrix() +  "      " + plat.getQuantite() + "\n";
            i++;
        }

        factureGenere += "          TPS:               " + tps() + "\n";
        factureGenere += "          TVQ:               " + tvq() + "\n";
        factureGenere += "          Le total est de:   " + total() + "\n";

        return factureGenere;
    }
}
