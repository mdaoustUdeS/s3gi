package ingredients;

import ingredients.exceptions.IngredientException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Ingredient {
    private String nom;
    private String description;
    private TypeIngredient typeIngredient;

    /**
     * Constructeur de la classe Ingredient
     *
     * @param   nom                     Nom de l'ingrédient
     * @param   description             Description de l'ingrédient
     * @throws  IngredientException     Exception lorsque la création de l'ingrédient ne fonctionne pas
     */
    public Ingredient(String nom, String description) throws IngredientException {
        if(!nom.equals("") && !description.equals("")) {
            this.nom = nom;
            this.description = description;
        }
        else {
            throw new IngredientException("Il est impossible de créer l'ingrédient avec les paramètres indiqués");
        }
    }

    /**
     * @return le nom de l'ingrédient
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom Nom de l'ingrédient
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return la description de l'ingrédient
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description Description de l'ingrédient
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return le type de l'ingrédient
     */
    public TypeIngredient getTypeIngredient() {
        return typeIngredient;
    }

    /**
     * @param typeIngredient Type de l'ingrédient
     */
    public void setTypeIngredient(TypeIngredient typeIngredient) {
        this.typeIngredient = typeIngredient;
    }
}
