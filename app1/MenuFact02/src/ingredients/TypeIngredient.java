package ingredients;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 *
 * Énumération des ingrédients
 */
public enum TypeIngredient {
    FRUIT, LEGUME, VIANDE, LAITIER, EPICE
}
