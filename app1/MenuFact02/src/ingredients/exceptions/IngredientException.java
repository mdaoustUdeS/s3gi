package ingredients.exceptions;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class IngredientException extends Exception{

    /**
     * Constructeur de la classe IngredientException
     *
     * @param message Message d'exception
     */
    public IngredientException(String message){
        super("IngredientException: " + message);
    }
}
