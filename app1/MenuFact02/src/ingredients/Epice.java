package ingredients;

import ingredients.exceptions.IngredientException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class Epice extends Ingredient {

    /**
     * Constructeur de la classe Épice
     *
     * @param   nom                     Nom de l'ingrédient
     * @param   description             Description de l'ingrédient
     * @throws  IngredientException     Exception lorsque la création de l'ingrédient ne fonctionne pas
     */
    public Epice(String nom, String description) throws IngredientException {
        super(nom, description);
        setTypeIngredient(TypeIngredient.EPICE);
    }
}
