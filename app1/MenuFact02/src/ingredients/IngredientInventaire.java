package ingredients;

import ingredients.exceptions.IngredientException;

/**
 * @author Karine Desrosiers (desk2812) et Matthieu Daoust (daom2504)
 * @since 2020-08-31
 * @version 2.0
 */
public class IngredientInventaire {
    private Ingredient ingredient;
    private int quantite;

    /**
     * Constructeur de la classe IngredientInventaire
     *
     * @param   ingredient              Ingrédient de l'inventaire
     * @param   quantite                Quantité de l'ingrédient dans l'inventaire
     */
    public IngredientInventaire(Ingredient ingredient, int quantite) {
        this.ingredient = ingredient;
        this.quantite = quantite;
    }

    /**
     * @return la quantité de l'ingrédient dans l'inventaire
     */
    public int getQuantite() {
        return quantite;
    }

    /**
     * @param   quantite                Quantité de l'ingrédient dans l'inventaire
     * @throws  IngredientException     Exception lorsque la quantité est négative
     */
    public void setQuantite(int quantite) throws IngredientException{
        if (quantite < 0)
            throw new IngredientException("Il n'est pas possible d'avoir une quantité négative");
        else
            this.quantite = quantite;
    }
}
