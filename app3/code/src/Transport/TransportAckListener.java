package Transport;

import PacketProcessor.*;

import java.io.IOException;
import java.net.*;

public class TransportAckListener extends Thread{

    private Transport transport = null;
    private DatagramSocket socket = null;

    private LinkDecodePacketProcessor receivingChain = null;

    public TransportAckListener(String name, DatagramSocket socket, Transport transport) {
        super(name);

        this.transport = transport;
        this.socket = socket;
    }

    public void run() {
        while (true) {
            try {
                byte[] buf = new byte[256];

                // receive request
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void CreateChain(boolean simulateError, DatagramSocket socket, InetAddress address, int port)
    {
        if (receivingChain == null) {
            receivingChain = new LinkDecodePacketProcessor();

//            PacketProcessor endOfChain = new LinkEncodePacketProcessor();
//            receivingChain.setNext(endOfChain);
//
//            if (simulateError) {
//                PacketProcessor error = new RandomErrorPacketProcessor();
//                endOfChain.setNext(error);
//                endOfChain = error;
//            }
//
//            endOfChain.setNext(new SocketPacketProcessor(socket, address, port));
        }
    }

}
