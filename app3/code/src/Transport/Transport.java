package Transport;

import PacketProcessor.*;
import java.util.ArrayList;
import java.util.Arrays;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;

public final class Transport {
    
    private static volatile Transport instance;

    private BufferedOutputStream outputStream;
    private TransportEncodePacketProcessor sendingChain;

    private int sequence;
    private int ack;

    public int getSequence() {
        return sequence;
    }

    public int getNextSequence() {
        synchronized (Transport.class) {
            return ++sequence;
        }
    }
//    public void setSequence(int sequence) {
//        this.sequence = sequence;
//    }

    public int getAck() {
        return ack;
    }

//    public void setAck(int ack) {
//        this.ack = ack;
//    }

    public void reset()
    {
        sequence = 0;
        ack = 0;
    }

    public void sendFile(BufferedInputStream file, String filename){
        if (sendingChain != null)
        {
            if (filename.length() > 200)
            {
                sendingChain.handleWithFlags(filename.substring(0, 200).getBytes(), getNextSequence(), true, false, true);
                sendingChain.handleWithFlags(filename.substring(200).getBytes(), getNextSequence(),false, false, true);
            } else {
                sendingChain.handleWithFlags(filename.getBytes(), getNextSequence(),true, false, true);
            }

            ArrayList<byte[]> packets = new ArrayList<byte[]>();
            ArrayList<Integer> sequences = new ArrayList<Integer>();

            try {
                byte[] packetData = new byte[200];

                int len = file.read(packetData, 0, 200);
                while (len != -1) {
                    int seq = getNextSequence();

                    if (len < 200) {
                        packetData = Arrays.copyOfRange(packetData, 0, len);

                        synchronized (Transport.class) {
                            sequences.add(seq);
                            packets.add(packetData);
                        }

                        sendingChain.handleWithFlags(packetData, seq, false,true,false);
                    } else {
                        synchronized (Transport.class) {
                            sequences.add(seq);
                            packets.add(packetData);
                        }

                        sendingChain.handleWithFlags(packetData, seq, false,false,false);
                    }

                    if (seq > getAck() + 10)
                    {
                        wait();
                    }

                    len = file.read(packetData, 0, 200);
                }
            } catch (java.io.IOException exception) {
                System.out.println(exception.getMessage());
            } catch (java.lang.InterruptedException exception2) {
                System.out.println(exception2.getMessage());
            }
        }
    }

    public void CreateChain(boolean simulateError, DatagramSocket socket, InetAddress address, int port)
    {
        if (sendingChain == null) {
            sendingChain = new TransportEncodePacketProcessor(this);

            PacketProcessor endOfChain = new LinkEncodePacketProcessor();
            sendingChain.setNext(endOfChain);

            if (simulateError) {
                PacketProcessor error = new RandomErrorPacketProcessor();
                endOfChain.setNext(error);
                endOfChain = error;
            }

            endOfChain.setNext(new SocketPacketProcessor(socket, address, port));
        }
    }



    private Transport() {

    }


    public static Transport getInstance()
    {
        Transport result = instance;
        if (result != null)
        {
            return result;
        }

        synchronized(Transport.class) {
            if(instance == null) {
                instance = new Transport();
            }
            return instance;
        }
    }
}
