package PacketProcessor;

public interface PacketProcessor {
    public void setNext(PacketProcessor next);
    public void handle(byte[] packet);
}
