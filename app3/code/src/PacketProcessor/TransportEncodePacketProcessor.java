package PacketProcessor;

import Transport.Transport;

public class TransportEncodePacketProcessor extends BasePacketProcessor {
    byte[] HEADER_LINE_1 = "SEQ ".getBytes();
    byte[] HEADER_LINE_2 = "ACK ".getBytes();
    byte[] HEADER_LINE_3 = "LEN ".getBytes();
    byte[] HEADER_LINE_4 = "MSG ".getBytes();
    byte[] HEADER_FILLER = "                ".getBytes();
    byte[] HEADER_NAME_FLAG = "name".getBytes();


    private Transport transport;

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public TransportEncodePacketProcessor(Transport transport)
    {
        this.transport = transport;
    }

    public void handle(byte[] packet)
    {
        handleWithFlags(packet, 0,false, false,false);
    }

    public void handleWithFlags(byte[] packet, int sequence, boolean first, boolean last, boolean filename)
    {
        byte[] header = new byte[64];


        System.arraycopy(HEADER_LINE_1, 0,
                header, 0, 4);

        System.arraycopy(String.format("%08d", sequence).getBytes(), 0,
                header, 4, 8);

        System.arraycopy(HEADER_FILLER, 0,
                header, 12, 4);



        System.arraycopy(HEADER_LINE_2, 0,
                header, 16, 4);

        System.arraycopy(HEADER_FILLER, 0,
                header, 20, 12);




        System.arraycopy(HEADER_LINE_3, 0,
                header, 32, 4);

        System.arraycopy(String.format("%03d", packet.length).getBytes(), 0,
                header, 36, 3);

        header[39] = 0x20; //space

        header [40] = (byte) (first ? 0x66 : 0x20); //f or space

        header[41] = 0x20; //space

        header [42] = (byte) (last ? 0x6c : 0x20); //l or space

        header[43] = 0x20; //space

        System.arraycopy(filename ? HEADER_NAME_FLAG : HEADER_FILLER, 0,
                header, 44, 4); //name of space


        System.arraycopy(HEADER_LINE_4, 0,
                header, 48, 4);

        System.arraycopy(HEADER_FILLER, 0,
                header, 52, 12);

        int headerLength = 64;
        int dataLength = packet.length;
        byte[] newPacket = new byte[headerLength + dataLength];
        System.arraycopy(header, 0, newPacket, 0, headerLength);
        System.arraycopy(packet, 0, newPacket, headerLength, dataLength);


        super.handle(newPacket);
    }

}
