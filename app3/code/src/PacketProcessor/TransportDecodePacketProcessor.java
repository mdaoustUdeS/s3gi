package PacketProcessor;

import java.util.Arrays;

public class TransportDecodePacketProcessor extends BasePacketProcessor {
    byte[] HEADER_LINE_1 = "SEQ ".getBytes();
    byte[] HEADER_LINE_2 = "ACK ".getBytes();
    byte[] HEADER_LINE_3 = "LEN ".getBytes();
    byte[] HEADER_LINE_4 = "MSG ".getBytes();
    byte[] HEADER_FILLER = "                ".getBytes();
    byte[] HEADER_FILLER_4 = "    ".getBytes();
    byte[] HEADER_NAME_FLAG = "name".getBytes();


    public void handle(byte[] packet)
    {
        if(packet.length < 64) {
            return;
        } else {
            if (!checkHeader(packet))
                return;

            int seq = Integer.parseInt(Arrays.copyOfRange(packet, 4, 11).toString());

            int ack = Integer.parseInt(Arrays.copyOfRange(packet, 20, 27).toString());

            int length = Integer.parseInt(Arrays.copyOfRange(packet, 36, 38).toString());

            boolean first = (packet[40] == 0x66); //f

            boolean last = (packet[42] != 0x6c); //l

            boolean filename = (Arrays.equals(Arrays.copyOfRange(packet, 44, 4), HEADER_NAME_FLAG));
        }
    }

    private boolean checkHeader(byte[] packet)
    {
        if (!Arrays.equals(Arrays.copyOfRange(packet, 0, 4), HEADER_LINE_1))
            return false;

        if (!Arrays.equals(Arrays.copyOfRange(packet, 16, 19), HEADER_LINE_2))
            return false;

        if (!Arrays.equals(Arrays.copyOfRange(packet, 32, 35), HEADER_LINE_3))
            return false;

        if (!Arrays.equals(Arrays.copyOfRange(packet, 48, 51), HEADER_LINE_4))
            return false;


        if (!Arrays.equals(Arrays.copyOfRange(packet, 12, 15), HEADER_FILLER_4))
            return false;

        if (!Arrays.equals(Arrays.copyOfRange(packet, 28, 31), HEADER_FILLER_4))
            return false;

        if (!Arrays.equals(Arrays.copyOfRange(packet, 44, 47), HEADER_FILLER_4))
            return false;

        return true;
    }
}
