package PacketProcessor;

public class BasePacketProcessor implements PacketProcessor{
    private PacketProcessor next;

    public void setNext(PacketProcessor nextPacketProcessor) {
        next = nextPacketProcessor;
    } 

    public void handle(byte[] packet)
    {
        if(next != null)
        {
            next.handle(packet);
        }
    }
}
