package PacketProcessor;

import java.io.*;
import java.net.*;

public class SocketPacketProcessor extends BasePacketProcessor {

    private DatagramSocket socket;
    private InetAddress address;
    private int port;

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress address) {
        if (address != null) {
            this.address = address;
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public void setSocket(DatagramSocket socket) {
        if (socket != null) {
            this.socket = socket;
        }
    }

    public SocketPacketProcessor(DatagramSocket socket, InetAddress address, int port)
    {
        this.socket = socket;
        this.address = address;
        this.port = port;
    }

    public void handle(byte[] packet)
    {
        try {
            if (packet != null) {
                DatagramPacket datagramPacket = new DatagramPacket(packet, packet.length, address, port);
                socket.send(datagramPacket);
            }
        } catch (java.io.IOException exception) {
            System.out.println(exception.getStackTrace());
        }
    }
}
