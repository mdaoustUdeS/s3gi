import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        FileServer.main(args);
        String[] tmpargs = {"one-liners.txt", "localhost"};
        FileClient.main(tmpargs);
    }
}
