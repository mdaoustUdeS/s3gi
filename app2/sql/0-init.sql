CREATE TABLE EventLog
(
  CIP CHAR(8) NOT NULL,
  ConnectTime timestamp NOT NULL,
  Operation VARCHAR(256) NOT NULL,
  EventID serial NOT NULL,
  PRIMARY KEY (EventID)
);

CREATE TABLE Departement
(
  DepartementName VARCHAR(128) NOT NULL,
  PRIMARY KEY (DepartementName)
);

CREATE TABLE Campus
(
  CampusName VARCHAR(128) NOT NULL,
  PRIMARY KEY (CampusName)
);

CREATE TABLE Pavillions
(
  PavillionName VARCHAR(128) NOT NULL,
  PavillionId CHAR(2) NOT NULL,
  CampusName VARCHAR(128) NOT NULL,
  PRIMARY KEY (PavillionId),
  FOREIGN KEY (CampusName) REFERENCES Campus(CampusName)
);

CREATE TABLE Characteristic
(
  CharacteristicID INT NOT NULL,
  Description VARCHAR(256) NOT NULL,
  PRIMARY KEY (CharacteristicID)
);

CREATE TABLE Status
(
  StatusName VARCHAR(32) NOT NULL,
  PRIMARY KEY (StatusName)
);

CREATE TABLE Category
(
  CategoryId INT NOT NULL,
  CategoryName VARCHAR(256) NOT NULL,
  PRIMARY KEY (CategoryId)
);

CREATE TABLE Privilege
(
  PrivilegeId INT NOT NULL,
  Description VARCHAR NOT NULL,
  PRIMARY KEY (PrivilegeId)
);

CREATE TABLE PrivilegeByCategory
(
  PrivilegeId INT NOT NULL,
  CategoryId INT NOT NULL,
  PRIMARY KEY (PrivilegeId, CategoryId),
  FOREIGN KEY (PrivilegeId) REFERENCES Privilege(PrivilegeId),
  FOREIGN KEY (CategoryId) REFERENCES Category(CategoryId)
);

CREATE TABLE PrivilegeByStatus
(
  StatusName VARCHAR(32) NOT NULL,
  PrivilegeId INT NOT NULL,
  PRIMARY KEY (StatusName, PrivilegeId),
  FOREIGN KEY (StatusName) REFERENCES Status(StatusName),
  FOREIGN KEY (PrivilegeId) REFERENCES Privilege(PrivilegeId)
);

CREATE TABLE PrivilegeByDepartement
(
  DepartementName VARCHAR(128) NOT NULL,
  PrivilegeId INT NOT NULL,
  PRIMARY KEY (DepartementName, PrivilegeId),
  FOREIGN KEY (DepartementName) REFERENCES Departement(DepartementName),
  FOREIGN KEY (PrivilegeId) REFERENCES Privilege(PrivilegeId)
);

CREATE TABLE Local
(
  LocalId VARCHAR(16) NOT NULL,
  Capacity INT NOT NULL,
  Notes VARCHAR(256),
  PavillionId CHAR(2) NOT NULL,
  Parent_LocalId VARCHAR(16),
  PRIMARY KEY (LocalId),
  FOREIGN KEY (PavillionId) REFERENCES Pavillions(PavillionId),
  FOREIGN KEY (Parent_LocalId) REFERENCES Local(LocalId)
);

CREATE TABLE UniUser
(
  CIP CHAR(8) NOT NULL,
  DepartementName VARCHAR(128) NOT NULL,
  PRIMARY KEY (CIP),
  FOREIGN KEY (DepartementName) REFERENCES Departement(DepartementName)
);

CREATE TABLE LocalCharacteristic
(
  CharacteristicID INT NOT NULL,
  LocalId VARCHAR(16) NOT NULL,
  PRIMARY KEY (CharacteristicID, LocalId),
  FOREIGN KEY (CharacteristicID) REFERENCES Characteristic(CharacteristicID),
  FOREIGN KEY (LocalId) REFERENCES Local(LocalId)
);

CREATE TABLE UserStatus
(
  CIP CHAR(8) NOT NULL,
  StatusName VARCHAR(32) NOT NULL,
  PRIMARY KEY (CIP, StatusName),
  FOREIGN KEY (CIP) REFERENCES UniUser(CIP),
  FOREIGN KEY (StatusName) REFERENCES Status(StatusName)
);

CREATE TABLE LocalCategory
(
  LocalId VARCHAR(16) NOT NULL,
  CategoryId INT NOT NULL,
  PRIMARY KEY (LocalId, CategoryId),
  FOREIGN KEY (LocalId) REFERENCES Local(LocalId),
  FOREIGN KEY (CategoryId) REFERENCES Category(CategoryId)
);

CREATE TABLE Reservation
(
  BeginTimestamp timestamp NOT NULL,
  EndTimestamp timestamp NOT NULL,
  CIP CHAR(8) NOT NULL,
  LocalId VARCHAR(16) NOT NULL,
  PRIMARY KEY (BeginTimestamp, LocalId),
  FOREIGN KEY (CIP) REFERENCES UniUser(CIP),
  FOREIGN KEY (LocalId) REFERENCES Local(LocalId)
);
