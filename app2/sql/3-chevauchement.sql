CREATE OR REPLACE FUNCTION nb_reservation_local(BeginTable timestamp, EndTable timestamp, localTable varchar(16))
RETURNS TABLE (begintimestamp timestamp, endtimestamp timestamp, localid VARCHAR(16), cip CHAR(8))
AS $variable_name$
SELECT *
FROM reservation
WHERE
      localid = localTable
  AND
      (BeginTable < endtimestamp AND EndTable > begintimestamp)
$variable_name$
LANGUAGE SQL;

SELECT count(*) from nb_reservation_local('2020-10-21 08:30:00', '2020-10-21 10:30:00', '1236');
SELECT count(*) from nb_reservation_local('2020-10-21 09:00:00', '2020-10-21 11:00:00', '1236');

CREATE OR REPLACE FUNCTION process_reservation_audit() RETURNS TRIGGER AS $eventlog$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO eventlog SELECT OLD.cip, now(), 'D';
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO eventlog SELECT NEW.cip, now(), 'U';
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$eventlog$ LANGUAGE plpgsql;

CREATE TRIGGER eventlog
AFTER INSERT OR UPDATE OR DELETE ON reservation
    FOR EACH ROW EXECUTE FUNCTION process_reservation_audit();

CREATE OR REPLACE FUNCTION res_validation() RETURNS trigger AS $res_validation$
    BEGIN
        IF (SELECT count(*) from nb_reservation_local(NEW.BeginTimestamp, NEW.EndTimestamp, New.LocalId)) != 0 THEN
            RAISE EXCEPTION 'There is already a reservation for that timeframe';
        END IF;
    END;
$res_validation$
LANGUAGE plpgsql;

CREATE TRIGGER res_validation BEFORE INSERT OR UPDATE ON reservation
    FOR EACH ROW EXECUTE FUNCTION res_validation();
