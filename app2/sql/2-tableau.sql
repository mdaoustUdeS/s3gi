CREATE OR REPLACE FUNCTION tableau(BeginTable timestamp, EndTable timestamp, CatTable INT)
RETURNS TABLE (begintimestamp timestamp, endtimestamp timestamp, localid VARCHAR(16), cip CHAR(8))
AS $variable_name$
    SELECT *
    FROM reservation
    WHERE begintimestamp >= BeginTable
        AND endtimestamp <= EndTable
        AND reservation.LocalId in (
            SELECT LocalId from LocalCategory WHERE CategoryId = CatTable
        )
$variable_name$
LANGUAGE SQL;

SELECT tableau(TIMESTAMP '2004-10-19 10:00:00', TIMESTAMP '2004-10-19 11:00:00', 1);
