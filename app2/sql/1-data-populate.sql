INSERT INTO campus (campusname) VALUES ('Sherby');
INSERT INTO pavillions (pavillionname, pavillionid, campusname) VALUES ('JABombardier', 'C2', 'Sherby');
INSERT INTO local (localid, capacity, notes, pavillionid) VALUES ('1234', 2, 'Plein', 'C2');
INSERT INTO local (localid, capacity, notes, pavillionid) VALUES ('1235', 2, 'Plein', 'C2');
INSERT INTO local (localid, capacity, notes, pavillionid) VALUES ('1236', 2, 'Plein', 'C2');
INSERT INTO local (localid, capacity, notes, pavillionid) VALUES ('1237', 2, 'Plein', 'C2');
INSERT INTO departement (departementname) VALUES ('GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('wars4561', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 1', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 2', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 3', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 4', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 5', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 6', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 7', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 8', 'GEGI');
INSERT INTO uniuser (cip, departementname) VALUES ('Evene 9', 'GEGI');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2004-10-19 10:15:00', '2004-10-19 10:45:00', 'wars4561', '1234');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2004-10-19 10:15:00', '2004-10-19 10:45:00', 'wars4561', '1235');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2008-10-19 10:16:00', '2008-10-19 10:45:00', 'wars4561', '1235');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2009-10-19 10:16:00', '2009-10-19 10:45:00', 'wars4561', '1235');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2003-10-19 10:16:00', '2009-10-19 10:45:00', 'wars4561', '1235');

INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 08:00:00', '2020-10-21 08:30:00', 'Evene 1', '1236');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 08:30:00', '2020-10-21 09:30:00', 'Evene 2', '1236');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 09:30:00', '2020-10-21 10:30:00', 'Evene 3', '1236');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 10:30:00', '2020-10-21 11:30:00', 'Evene 4', '1236');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 11:30:00', '2020-10-21 12:00:00', 'Evene 5', '1236');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 07:30:00', '2020-10-21 12:30:00', 'Evene 6', '1236');

INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 08:30:00', '2020-10-21 10:30:00', 'Evene 7', '1237');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 08:30:00', '2020-10-21 11:00:00', 'Evene 8', '1235');
INSERT INTO reservation (begintimestamp, endtimestamp, cip, localid) VALUES ('2020-10-21 08:00:00', '2020-10-21 10:30:00', 'Evene 9', '1237');

INSERT INTO category (categoryid, categoryname) VALUES (0, 'DemoLocal');
INSERT INTO localcategory (localid, categoryid) VALUES ('1234', 0);
INSERT INTO localcategory (localid, categoryid) VALUES ('1235', 0);
INSERT INTO localcategory (localid, categoryid) VALUES ('1236', 0);

select * from reservation;
