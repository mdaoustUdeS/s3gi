---
fontsize: 12pt
lang: fr
lieu: |
      | UNIVERSITÉ DE SHERBROOKE
      | Faculté de génie
      | Département de génie électrique et génie informatique
title: Examen -- Les déchets électroniques
course-title: |
              | Ingénieur et société
              | GEN400
presented-to: Équipe de formateurs de la session S3
author: Matthieu Daoust, daom2504
date: Sherbrooke, 11 décembre 2020
toc: true
geometry: margin=1.5in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{2}
    - \usepackage[all]{nowidow}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \cfoot{\thepage}
---

# Introduction

## Sujet amené

Depuis que l'être humain a découvert qu'il pouvait produire des appareils électroniques, il en a consommé. Lorsque ces appareils arrivent en fin de vie, ils produisent des déchets.

## Sujet posé

On se demande s'il existe une façon d'éliminer ces déchets.

## Sujet divisé

Dans ce texte, il sera question de la présentation de la solution et de la justification du projet.

\newpage

# Développement

## Présentation synthétisée et vulgarisée de la solution

D'abord, la solution choisie est la diminution à la source la production d'appareils qui produisent des déchets d'équipements électriques et électroniques. En diminuant la production et en augmentant les prix, moins de déchets seraient produits, ce qui ferait en sorte qu'il y aurait moins de déchets à éliminer.

## Justification du projet pilote

Selon les quatre valeurs fondamentales du développement durable (Villeneuve 1998), le projet pilote répondrait parfaitement à ces quatre valeurs.

1. Ce serait écologiquement viable, car moins de ressources seraient utilisées.

2. Cela permettrait de réduire les disparités entre les riches et les pauvres, car tous utiliseraient moins de ressources.

3. Ce serait économiquement efficace, car les entreprises pourraient augmenter les prix sur les articles.

4. Ce serait socialement équitable, car tous auraient accès aux mêmes produits.

\newpage

# Conclusion

## Rappel

En conclusion, il serait bénéfique de réduire à la source la production d'appareils qui produisent des déchets d'équipements électriques et électroniques.

## Ouverture

Seriez-vous prêt à essayer ce projet pilote?

\newpage

# Bibliographie

## Textes

Villeneuve, Claude. 1998. _Qui a peur de l’an 2000?  Guide d’éducation relative à l’environnementpour le développement durable._ Sainte-Foy: Éditions Multimondes et UNESCO. 303 p.
