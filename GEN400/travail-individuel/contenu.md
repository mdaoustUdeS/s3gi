---
fontsize: 12pt
lang: fr
lieu: |
      | UNIVERSITÉ DE SHERBROOKE
      | Faculté de génie
      | Département de génie électrique et génie informatique
title: Travail individuel sur l'implémentation des cartes de contrôle d'accès dans un bâtiment
course-title: |
              | Ingénieur et société
              | GEN400
presented-to: Équipe de formateurs de la session S3
author: Matthieu Daoust, daom2504
date: Sherbrooke, 6 novembre 2020
toc: true
geometry: margin=1.5in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{2}
    - \usepackage[all]{nowidow}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \cfoot{\thepage}
---

# Introduction

## Sujet amené

Depuis longtemps, les entreprises souhaitent contrôler l'accès à leur propriété. Ils ont longtemps utilisé des serrures mécaniques jumelées avec des clés pour y parvenir.

## Sujet posé

On se demande si elles devraient recourir à l'implémentation de cartes de contrôle d'accès dans leur bâtiment.

## Sujet divisé

Dans ce texte, il sera question du cas choisi, de la controverse, des thématiques impliquées et du diagnostic.

\newpage

# Développement

## Description du cas choisi

### Historique

Les entreprises ont longtemps recouru à des serrures et des clés pour contrôler l'accès à leurs actifs. Ils ont maintenant le choix d'utiliser d'autres moyens technologiques pour contrôler l'accès.

### Groupes et acteurs impliqués

Plusieurs acteurs sont impliqués dans l'installation, l'utilisation et la gestion des outils de contrôle d'accès.

Entre autres, il y a les personnes qui ont besoin d'accéder aux actifs. Ce sont souvent des employés de l'entreprise à laquelle appartiennent les actifs.

Il y a aussi les personnes qui sont responsables de contrôler l'accès aux actifs et de veiller à la sécurité de ceux-ci. Ce sont souvent des agents de sécurité ou des contractants qui viennent tester des vulnérabilités.

Enfin, il peut y avoir les personnes responsables d'implémenter le système de contrôle d'accès. Ce sont souvent des professionnels, tels que des serruriers ou des installateurs de systèmes de sécurité.

### Technologies utilisées

Les personnes qui ont besoin d'accéder aux actifs utilisent un dispositif pour y parvenir. Pour pouvoir accéder aux actifs, ce dispositif doit être compatible avec le mécanisme de protection.

Souvent, lorsque le mécanisme de protection est une serrure mécanique, le dispositif d'accès est une clé mécanique.

Sinon, lorsque le mécanisme de protection est une serrure électronique, le dispositif d'accès est une clé électronique.

### Définition des concepts techniques

Le dispositif technologique permet d'authentifier la personne qui souhaite accéder aux actifs.

### Lieu d’implantation ou de transfert

Le mécanisme de protection est souvent implémenté conjointement avec des mesures de protections physiques.

Le dispositif technologique est souvent à proximité de la personne qui souhaite accéder aux actifs.

## Description de la controverse

### Historique

La controverse est apparue avec l'arrivée des systèmes de contrôles d'accès avec des cartes magnétiques.

### Positions adverses

Bien qu'il y ait plusieurs avantages à utiliser un système de contrôle d'accès avec des cartes magnétiques, cela peut emmener certains risques. En effet, le système de contrôle d'accès pourrait être utilisé pour surveiller le temps de travail des employés.

### Groupes et acteurs impliqués dans la controverse

La majorité des acteurs impliqués dans le cas choisi sont aussi impliqués dans la controverse.

### Valeurs en conflit

Des valeurs sont en conflits dans cette controverse. Entre autres, la liberté dans l'exécution des tâches par les employés peut s'opposer aux besoins de contrôle exercés par l'entreprise.

## Thématiques impliquées

Plusieurs thèmes sont impliqués dans ce cas.

### Rapport au temps

On peut voir le rapport au temps «Chronos» dans le travail des employés. En effet, le temps de travail des employés est souvent mesuré.

On peut aussi voir le rapport au temps «Kairos» dans l'installation d'un système de sécurité. En effet, les systèmes de sécurité sont souvent installés très tôt dans la vie d'un bâtiment. Ceci est souvent le bon moment puisque, lorsqu'il n'y a pas de dispositifs de sécurité installés, le risque de subir des pertes augmente avec le temps.

### Organisation du travail

Il est possible que l'utilisation de cartes d'accès permettent de modifier l'organisation du travail. En effet, il est possible de configurer les systèmes de contrôle d'accès pour mesurer le temps de travail des employés de façon automatique.

### Développement durable

L'implémentation de cartes de contrôle d'accès peut avoir un impact sur le développement durable. En effet, en considérant que les systèmes de contrôle d'accès modernes utilisent des matériaux différents et qu'ils permettent de diminuer la quantité de clés, il est important de faire une analyse du cycle de vie afin d'évaluer l'ensemble des impacts environnementaux de l'implémentation d'un tel système.

## Diagnostic en fonction de l’analyse de la controverse

Ce projet n’est acceptable qu’à la condition que des modifications soient apportées. En effet, il est probablement bénéfique d'utiliser un système de contrôle d'accès avec des cartes magnétiques, à condition que le système ne serve pas à mesurer le temps de travail des employés.

\newpage

# Conclusion

## Résumé

En conclusion, les cartes de contrôles d'accès devraient être utilisés partout, à condition que ce soit fait dans le respect de plusieurs critères.

## Ouverture

Utiliserez-vous des cartes de contrôle d'accès?

\newpage

# Bibliographie

## Sites webs

https://www.isonas.com/news-education/the-evolution-of-access-control/

## Encyclopédie

https://en.wikipedia.org/wiki/Access_control

https://fr.wikipedia.org/wiki/Serrure
